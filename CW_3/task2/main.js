//    ============
//    ====class===
//    ============

//    - є сторінка rules.html. Контентом сторінки є заголовки та параграфи. Заголовки (h2)
//     характеризують тему контенту яка вказана в параграфі.
//    створити скрипт, котрий зчитує всі заголовки, та робить в блоці з id=content з них
//    список(ul>li), який буде змістом того, що знаходиться на сторінці.
//    Скріпт повинен працювати навіть якщо кількість блоків з заголовком та параграфом зміниться.
// const content = document.getElementById('content');
// const h1 = document.getElementById('h1');
// const allRulesDiv = document.getElementsByClassName('rules');
// const createUl = document.createElement('ul');
// const newH1 = document.createElement('h1');
// createUl.classList.add('ul');
// newH1.innerText = h1.innerText;
// content.appendChild(newH1);
// content.appendChild(createUl);

// for (const divItem of allRulesDiv) {
//     const liItems = divItem.getElementsByTagName('h2');

//     for (const liItem of liItems) {
//         const createLi = document.createElement('li');
//         createLi.innerText = liItem.innerText;
//         createLi.classList.add('li');
//         createUl.appendChild(createLi);
//     }
// }
//

//    -Є масив котрий характеризує правила. Створити скрипт який ітерує цей масив, та робить
//     з кожне правило в окремому блоці.
//    При цому в блоці, номер правила записати в свій блок, текст правила записати в свій
//    окремий блок.
//    Результатом відпрацювання скріпта повинна бути структура яка міститься в блоці wrap
//    файла rule.html
let rules = [
    {
        title: 'Первое правило Бойцовского клуба.',
        body: 'Никому не рассказывать о Бойцовском клубе.',
    },
    {
        title: 'Второе правило Бойцовского клуба.',
        body: 'Никогда никому не рассказывать о Бойцовском клубе.',
    },
    {
        title: 'Третье правило Бойцовского клуба.',
        body: 'В схватке участвуют только двое.',
    },
    {
        title: 'Четвертое правило Бойцовского клуба.',
        body: 'Не более одного поединка за один раз.',
    },
    {
        title: 'Пятое правило Бойцовского клуба.',
        body: 'Бойцы сражаются без обуви и голые по пояс.',
    },
    {
        title: 'Шестое правило Бойцовского клуба.',
        body: 'Поединок продолжается столько, сколько потребуется.',
    },
    {
        title: 'Седьмое правило Бойцовского клуба.',
        body:
            'Если противник потерял сознание илиговорит «Хватит» — поединок окончен.',
    },
    {
        title: 'Восьмое и последнее правило Бойцовского клуба.',
        body: 'Новичок обязан принять бой.',
    },
];

// for (const rule of rules) {
//     const ruleWraper = document.createElement('div');
//     const ruleTitle = document.createElement('div');
//     const ruleBody = document.createElement('div');

//     ruleWraper.classList.add('ruleWraper');
//     document.body.appendChild(ruleWraper);

//     ruleTitle.classList.add('body');
//     ruleTitle.innerText = rule.title;
//     ruleWraper.appendChild(ruleTitle);

//     ruleBody.classList.add('rule');
//     ruleBody.innerText = rule.body;
//     ruleWraper.appendChild(ruleBody);
// }

//

//================================= ****** =================================

//    *** за допомогою fetch (як в прикладі) отримати від jsonplaceholder всі users.
//    За допомогою document.createElement вивести їх в браузер. Помістити кожен окремий
//    об'єкт в блок, при цьому кожен внутрішній об'єкт в свій блок (блок в блоці).
//    *** за допомогою fetch (як в прикладі) отримати від jsonplaceholder всі posts.
//    За допомогою document.createElement вивести їх в браузер. Помістити кожен окремий
//    об'єкт в блок, при цьому кожен внутрішній об'єкт(якщо він існує) в свій блок (блок в блоці).
//    *** за допомогою fetch (як в прикладі) отримати від jsonplaceholder всі comments.
//    За допомогою document.createElement вивести їх в браузер. Помістити кожен окремий
//    об'єкт в блок, при цьому кожен внутрішній об'єкт(якщо він існує) в свій блок (блок в блоці).
//    ****** при помощи fetch (как в примере) получить от jsonplaceholder все posts.
//    Внутри последнего then() сделать еще один fetch который сделает запрос и получит
//    все comments. Объеденить соответсвующий post с соответсвующими comment и вывести в
//    браузер. Подсказка : в каждом comment есть поле postId которое определяет какой
//    комментарий принадлежит какому посту
