// 1.Определите переменные str, num, flag и txt со значениями «Привет», 123, true, «true».
// При помощи оператора typeof убедитесь, что значения переменных принадлежат типам: string, number и  boolean.

let str = 'Привет';
//let num = 123;
let flag = true;
let txt = 'true';
// console.log(typeof str);
// console.log(typeof num);
// console.log(typeof flag);
// console.log(typeof txt);

// 2. Создайте переменные a1, a2, a3, a4, a5. При помощи математических операторов (сложение, вычитание и т.д.)
// найдите значения выражений:
// 5 + 3,
// 5 - 3,
// 5 * 3,
// 5 / 3,
// 5 % 3
// поместив результат каждого выражения в соответствующую переменную. Например, let a1 = 5 + 3.


let a1 = 5 + 3;
let a2 = 5 - 3;
let a3 = 5 * 3;
let a4 = 5 / 3;
let a5 = 5 % 3;

// 3.  Создайте переменные a6, a7, a8, a9, a10. Поместите в них результат выражений:
//     5 % 3,
//     3 % 5,
//     5 + '3',
//     '5' - 3,
//     75 + 'кг'

let a6 = 5 % 3;
let a7 = 3 % 5;
let a8 = 5 + '3';
let a9 = '5' - 3;
let a10 = 75 + 'кг';

// 4. Напишите код, который находит площадь прямоугольника высота 23см. ( переменная height),
//   шириной 10см (переменная width), значение площади должно хранится в числовой переменной s.

let height = 23;
let width = 10;
let s = height * width;
console.log(`${s} см^2`);

// 5.  Напиши код, который находит объем цилиндра высотой 10м (переменная heightC) и диаметром основания 4м (dC),
// результат поместите в переменную v.

let heightC = 10;
let dC = 4;
const pi = 3.14;
let v = pi * height * Math.pow(dC / 2, 2);
console.log(v);

// 6. У прямоугольного треугольника две стороны n (со значением 3) и m (со значением 4).
//   Найдите гипотенузу k по теореме Пифагора (нужно использовать функцию Math.pow(число, степень) или оператор
// возведения в степень ** ).

let n = 3;
let m = 4;
let k = Math.sqrt(Math.pow(n, 2) + Math.pow(m, 2));
console.log(k);

// 7. Напишите скрипт, который выводит "Hello world", создавши переменную str и выводить спомощью document.write,
// alert и console.log

let strt = 'Hello world';
// document.write(strt);
// alert(strt);
// console.log(strt);

// 8. Вывести в окно браузера при помощи метода alert() следующие данные: Ваше ФИО, возраст, хобби (каждой на новой
// строки спомощью \n).

let allName = 'Феденко Максим Володимирович';
let age = 24;
let hobby = 'Football';
// alert(`${allName} \n ${age} \n ${hobby}`);

// 9. Создать 4 переменные с использованием ключевого слова let с именами str1, str2, str3, concatenation.
//   Переменной str1 присвоить фразу ‘Кто ‘, str2 – ‘ты ‘, str3 – ‘такой?’
//   Локальной переменной concatenation присвоить результат конкатенации 3-х строк: str1, str2, str3.
//   Вывести в документ содержимое переменной concatenation спомощью document.write

let str1 = 'Кто';
let str2 = 'ты';
let str3 = 'такой';
let concatenation = `${str1} ${str2} ${str3}`;
console.log(concatenation);

// 10. Какие значения выведет в окно браузера следующий фрагмент кода?  и почему?
//     let str = "20";
//      let a = 5;
//      document.write(str + a + "<br/>");
//      document.write(str - a + "<br/>");
//      document.write(str * "2" + "<br/>");
//      document.write(str / 2 + "<br/>");

// 11. Какие значения выведет в окно консоли следующий фрагмент кода если его поместить в console.log?
//     parseInt("3.14")
//     parseInt("-7.875")
//     parseInt("435")
//     parseInt("Вася")

// 12.  С помощью окна ввода, вызываемого методом prompt, пользователь может ввести данные, которые будут
// использоваться далее, повторите код ниже
//     let str = prompt("Enter something", "ho-ho")
//     console.log(str);

// 13. С помощью окна ввода, вызываемого методом prompt, сделать сложение двух чисел, а вывод результата при помощи
// метода alert
// 14. С помощью окна ввода, вызываемого методом prompt, пользователь последовательно выводит имя, фамилию и
// возраст, а вам не обходимо вывести строку такого вида
//       Доброго вечера Иван Иванов, мои поздравления что вам 32 , а вывод результата при помощи метода alert

//

//

//
// =====================
// ======ДОП============
// =====================

// 1. Три різних числа вводяться через prompt().
// За допомоги if else вивести іх в порядку зростання. (відсортувати по зростанню)

//let a = +prompt('Введіть число №1');
// let b = +prompt('Введіть число №2');
// let c = +prompt('Введіть число №3');

// if (a <= b && b <= c) {
//     console.log(a, b, c);
// } else if (b <= a && a <= c) {
//     console.log(b, a, c);
// } else if (b <= c && c <= a) {
//     console.log(b, c, a);
// } else if (c <= b && b <= a) {
//     console.log(c, b, a);
// } else if (c <= a && a <= b) {
//     console.log(c, a, b);
// } else if (a <= c && c <= b) {
//     console.log(a, c, b);
// }

let num1 = +prompt('Введіть число №1');
function makeNegative(num) {
    if (Math.sign(num) === -1) {
        console.log(num);
    } else if (Math.sign(num) === 1) {
        console.log(-num);
    } else {
        console.log(num);
    }
}
makeNegative(num1);

// let minNum = Math.min(num1, num2, num3);
// let center = Math.
// let maxNum = Math.max(num1, num2, num3);

// console.log(minNum);

// if (num1 < num2 && num1 < num3) {
//     console.log(num1);
//     if (num2 < num3) {
//         console.log(num2);
//         console.log(num3);
//     } else {
//         console.log(num3);
//         console.log(num2);
//     }
// } else if (num2 < num1 && num2 < num3) {
//     console.log(num2);
//     if (num1 < num3) {
//         console.log(num1);
//         console.log(num3);
//     } else {
//         console.log(num3);
//         console.log(num1);
//     }
// } else if (num3 < num1 && num3 < num2) {
//     console.log(num3);
//     if (num2 < num1) {
//         console.log(num2);
//         console.log(num1);
//     } else {
//         console.log(num1);
//         console.log(num2);
//     }
// } else {
//     console.log('Сталася помилка!');
// }

//

//

// 2.
// Все параматры получаем с клавиатуры!!!!
// Имитируем поведение пешехода на перекстке.
// Если светофор зеленый - вывести "иди".
// Если светофор желтый - вывести "подожди".
// Если светофор красный - вывести "стой".
// Если светофор в аварийном режиме вывести "делай что хочешь"!

// let color = prompt('Введите цвет светофора для получения дальнейших рекомендаций',);

// switch (color) {
//     case 'зеленый':
//         console.log('иди');
//         break;
//     case 'желтый':
//         console.log('подожди');
//         break;
//     case 'красный':
//         console.log('стой');
//         break;
//     default:
//         console.log('делай что хочешь');
//         break;
// }

//

//

// 3
// Все параметры получаем с клавиатуры!!!!(prompt , confirm)
// Создать переменную isRoadClear которая характеризирует наличие на дороге машин.
// Улучшаем предыдущее задание.
// Если светофор зеленый и машин нет - вывести "иди".
// Если светофор зеленый и машины есть  - вывести подожди пока нарушители проедут".
// Если светофор желтый и машины есть - вывести "жди".
// Если светофор желтый и машин нет - вывести "все рано жди".
// Если светофор красный и машин нет- вывести "стой все рано".
// Если светофор красный - и машины есть вывести "стой и жди".
// Если светофор в аварийном режиме вывести "делай что хочешь"!

let color = prompt('Введите цвет светофора для получения дальнейших рекомендаций',);
let isRoadClear = confirm('Машины есть?');

switch ((color, isRoadClear)) {
    case ('зеленый', false):
        console.log('иди');
        break;
    case ('зеленый', true):
        console.log('подожди пока нарушители проедут');
        break;
    case ('желтый', false):
        console.log('все рано жди');
        break;
    case ('желтый', true):
        console.log('жди');
        break;
    case ('красный', false):
        console.log('стой все рано');
        break;
    case ('красный', true):
        console.log('стой и жди');
        break;
    default:
        console.log('делай что хочешь');
        break;
}


