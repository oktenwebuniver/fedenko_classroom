let cars = [
    {
        producer: 'subaru',
        model: 'wrx',
        year: 2010,
        color: 'blue',
        type: 'sedan',
        engine: 'ej204x',
        volume: 2,
        power: 400,
    },
    {
        producer: 'subaru',
        model: 'legacy',
        year: 2007,
        color: 'silver',
        type: 'sedan',
        engine: 'ez30',
        volume: 3,
        power: 250,
    },
    {
        producer: 'subaru',
        model: 'tribeca',
        year: 2011,
        color: 'white',
        type: 'jeep',
        engine: 'ej20',
        volume: 2,
        power: 300,
    },
    {
        producer: 'subaru',
        model: 'leone',
        year: 1998,
        color: 'yellow',
        type: 'sedan',
        engine: 'ez20x',
        volume: 2,
        power: 140,
    },
    {
        producer: 'subaru',
        model: 'impreza',
        year: 2014,
        color: 'red',
        type: 'sedan',
        engine: 'ej204x',
        volume: 2,
        power: 200,
    },
    {
        producer: 'subaru',
        model: 'outback',
        year: 2014,
        color: 'red',
        type: 'hachback',
        engine: 'ej204',
        volume: 2,
        power: 165,
    },
    {
        producer: 'bmw',
        model: '115',
        year: 2013,
        color: 'red',
        type: 'hachback',
        engine: 'f15',
        volume: 1.5,
        power: 120,
    },
    {
        producer: 'bmw',
        model: '315',
        year: 2010,
        color: 'white',
        type: 'sedan',
        engine: 'f15',
        volume: 1.5,
        power: 120,
    },
    {
        producer: 'bmw',
        model: '650',
        year: 2009,
        color: 'black',
        type: 'coupe',
        engine: 'f60',
        volume: 6,
        power: 350,
    },
    {
        producer: 'bmw',
        model: '320',
        year: 2012,
        color: 'red',
        type: 'sedan',
        engine: 'f20',
        volume: 2,
        power: 180,
    },
    {
        producer: 'mercedes',
        model: 'e200',
        year: 1990,
        color: 'silver',
        type: 'sedan',
        engine: 'eng200',
        volume: 2,
        power: 180,
    },
    {
        producer: 'mercedes',
        model: 'e63',
        year: 2017,
        color: 'yellow',
        type: 'sedan',
        engine: 'amg63',
        volume: 3,
        power: 400,
    },
    {
        producer: 'mercedes',
        model: 'c250',
        year: 2017,
        color: 'red',
        type: 'sedan',
        engine: 'eng25',
        volume: 2.5,
        power: 230,
    },
];
// // Відфільтрувати масив за наступними крітеріями :
// // - двигун більше 3х літрів
// const manyPow = cars.filter((value) => value.volume > 3);
// // console.log(manyPow);

// //

// // - двигун = 2л
// const midlPow = cars.filter((value) => value.volume === 2);
// // console.log(midlPow);

// //

// // - виробник мерс
// const madeMercedes = cars.filter((value) => value.producer === 'mercedes');
// // console.log(madeMercedes);

// //

// // - двигун більше 3х літрів + виробник мерседес
// const producerMercedesMaxPow = cars.filter(
//     (value) => value.volume > 3 && value.producer === 'mercedes',
// );
// // console.log(producerMercedesMaxPow);

// //

// // - двигун більше 3х літрів + виробник субару
// const producerSubaruMaxPow = cars.filter(
//     (value) => value.volume > 3 && value.producer === 'subaru',
// );
// // console.log(producerSubaruMaxPow);

// //

// // - сили більше ніж 300
// const pow300 = cars.filter((value) => value.power > 300);
// // console.log(pow300);

// //

// // - сили більше ніж 300 + виробник субару
// const producerSubaruPow300 = cars.filter(
//     (value) => value.power > 300 && value.producer === 'subaru',
// );
// // console.log(producerSubaruPow300);

// //

// // - мотор серіі ej
// const ejEngines = cars.filter((value) => value.engine.startsWith('ej'));
// // console.log(ejEngines);

// //

// // - сили більше ніж 300 + виробник субару + мотор серіі ej
// const ejEnginesProdSubaruPow300 = cars.filter(
//     (value) =>
//         value.engine.includes('ej') &&
//         value.producer === 'subaru' &&
//         value.power > 300,
// );
// // console.log(ejEnginesProdSubaruPow300);

// //

// // - двигун меньше 3х літрів + виробник мерседес
// const prodMercMidlePow = cars.filter(
//     (value) => value.producer === 'mercedes' && value.volume < 3,
// );
// // console.log(prodMercMidlePow);

// //

// // - двигун більше 2л + сили більше 250
// const power250 = cars.filter((value) => value.power > 250 && value.volume > 2);
// // console.log(power250);

// //

// // - сили більше 250  + виробник бмв
// const producerBMWpower250 = cars.filter(
//     (value) => value.power > 250 && value.producer === 'bmw',
// );
// // console.log(producerBMWpower250);

//

//==================================================================================

// - взять слдующий массив
let usersWithAddress = [
    {
        id: 1,
        name: 'vasya',
        age: 31,
        status: false,
        address: { city: 'Lviv', street: 'Shevchenko', number: 16 },
    },
    {
        id: 2,
        name: 'petya',
        age: 30,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 1 },
    },
    {
        id: 3,
        name: 'kolya',
        age: 29,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 121 },
    },
    {
        id: 4,
        name: 'olya',
        age: 28,
        status: false,
        address: { city: 'Lviv', street: 'Shevchenko', number: 90 },
    },
    {
        id: 5,
        name: 'max',
        age: 30,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 115 },
    },
    {
        id: 6,
        name: 'anya',
        age: 31,
        status: false,
        address: { city: 'Lviv', street: 'Shevchenko', number: 2 },
    },
    {
        id: 7,
        name: 'oleg',
        age: 28,
        status: false,
        address: { city: 'Lviv', street: 'Shevchenko', number: 22 },
    },
    {
        id: 8,
        name: 'andrey',
        age: 29,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 43 },
    },
    {
        id: 9,
        name: 'masha',
        age: 30,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 12 },
    },
    {
        id: 10,
        name: 'olya',
        age: 31,
        status: false,
        address: { city: 'Lviv', street: 'Shevchenko', number: 16 },
    },
    {
        id: 11,
        name: 'max',
        age: 31,
        status: true,
        address: { city: 'Lviv', street: 'Shevchenko', number: 121 },
    },
];
// -- отсортировать его по id пользователей
// const sortId1 = JSON.parse(JSON.stringify(usersWithAddress));
// sortId1.sort((a, b) => a.id > b.id);
// // console.log(sortId1);

// // -- отсортировать его по id пользователей в обратном опрядке
// const sortId2 = JSON.parse(JSON.stringify(usersWithAddress));
// sortId2.sort((a, b) => a.id < b.id);
// // console.log(sortId2);

// // -- отсортировать его по возрасту пользователей
// const sortAge1 = JSON.parse(JSON.stringify(usersWithAddress));
// sortAge1.sort((a, b) => a.age < b.age);
// // console.log(sortAge1);

// // -- отсортировать его по возрасту пользователей в обратном порядке
// const sortAge2 = JSON.parse(JSON.stringify(usersWithAddress));
// sortAge1.sort((a, b) => a.age > b.age);
// // console.log(sortAge2);

// // -- отсортировать его по имени пользователей
// const sortName1 = JSON.parse(JSON.stringify(usersWithAddress));
// sortName1.sort((a, b) => a.name > b.name);
// // console.log(sortName1);

// // -- отсортировать его по имени пользователей в обратном порядке
// const sortName2 = JSON.parse(JSON.stringify(usersWithAddress));
// sortName2.sort((a, b) => a.name < b.name);
// // console.log(sortName2);

// // -- отсортировать его по названию улицы  в алфавитном порядке
// const sortStreetName = JSON.parse(JSON.stringify(usersWithAddress));
// sortStreetName.sort((a, b) => a.address.street > b.address.street);
// // console.log(sortStreetName);

// // -- отсортировать его по номеру дома по возрастанию
// const sortStreetNum = JSON.parse(JSON.stringify(usersWithAddress));
// sortStreetNum.sort((a, b) => a.address.number > b.address.number);
// // console.log(sortStreetNum);

// // -- отфильтровать (оставить) тех кто младше 30
// const sortYang = usersWithAddress.filter((value) => value.age < 30);
// // console.log(sortYang);

// // -- отфильтровать (оставить) тех у кого отрицательный статус
// const sortStatFalse = usersWithAddress.filter(
//     (value) => value.status === false,
// );
// // console.log(sortStatFalse);

// // -- отфильтровать (оставить) тех у кого отрицательный статус и младше 30 лет
// const sortStatFalseYang = usersWithAddress.filter(
//     (value) => value.status === false && value.age < 30,
// );
// // console.log(sortStatFalseYang);

// // -- отфильтровать (оставить) тех у кого номер дома четный
// const sortNumberHome = usersWithAddress.filter(
//     (value) => value.address.number % 2 === 0,
// );
// // console.log(sortNumberHome);

//

//

// ================================================
// ================ДОПОЛНИТЕЛЬНО===================
// ================================================

// Створити обєкт автомобіля з полями:
// Марка автомобля, потужність двигуна, власник, ціна, рік випуску.
// Власник автомобіля теж має бути обєкт, у якого є поля
// Імя, вік, стаж водіння.
// Створити не менше 7 та не більше 20 машинок.

const myCars = [
    {
        model: 'bentli',
        madeYear: 2015,
        power: 420,
        price: 55000,
        driver: { name: 'Max', age: 32, gender: 'man', experience: 3 },
    },
    {
        model: 'tesla:',
        madeYear: 2020,
        power: 355,
        price: 65000,
        driver: { name: 'Eduard', age: 28, gender: 'man', experience: 5 },
    },
    {
        model: 'vaz',
        madeYear: 2010,
        power: 212,
        price: 30000,
        driver: { name: 'Alfred', age: 35, gender: 'man', experience: 12 },
    },
    {
        model: 'audi',
        madeYear: 2012,
        power: 263,
        price: 15000,
        driver: { name: 'Vika', age: 18, gender: 'woman', experience: 0 },
    },
    {
        model: 'niva',
        madeYear: 2005,
        power: 200,
        price: 51000,
        driver: { name: 'Alina', age: 25, gender: 'woman', experience: 7 },
    },
    {
        model: 'opel',
        madeYear: 2015,
        power: 287,
        price: 25000,
        driver: { name: 'Anton', age: 27, gender: 'man', experience: 1 },
    },
    {
        model: 'zaz',
        madeYear: 2010,
        power: 154,
        price: 12000,
        driver: { name: 'Dima', age: 30, gender: 'man', experience: 4 },
    },
    {
        model: 'mitsubisi',
        madeYear: 2015,
        power: 312,
        price: 5000,
        driver: { name: 'Ann', age: 18, gender: 'woman', experience: 0 },
    },
    {
        model: 'alfraomeo',
        madeYear: 2018,
        power: 385,
        price: 7000,
        driver: { name: 'Adolf', age: 20, gender: 'man', experience: 8 },
    },
    {
        model: 'toyota',
        madeYear: 2003,
        power: 208,
        price: 18000,
        driver: { name: 'Lisa', age: 25, gender: 'woman', experience: 5 },
    },
];
const newDrivers = [
    { name: 'Anton', age: 18, gender: 'trans', experience: 5 },
    { name: 'Tamara', age: 29, gender: 'woman', experience: 3 },
    { name: 'Roni', age: 23, gender: 'man', experience: 2 },
    { name: 'Jorg', age: 2, gender: 'man', experience: 7 },
    { name: 'Sonya', age: 23, gender: 'woman', experience: 2 },
    { name: 'Taras', age: 27, gender: 'man', experience: 4 },
    { name: 'Olya', age: 15, gender: 'woman', experience: 5 },
    { name: 'Toni', age: 26, gender: 'man', experience: 0 },
];
// Зробили половину автопарку ремонт мотору, що збільшить потужність автомобілів на 10% (переприсвоєння змінної
// потужності).
const halfCars = myCars.filter((value, index) => index % 2);
halfCars.forEach((value) => {
    value.power = Math.round(value.power * 1.1);
});
// На відремонтовані автомобілі найняти нових водіїв (переприсвоїти змінну водій).
newDrivers.splice(halfCars.length);
halfCars.forEach((value, index) => {
    newDrivers.forEach((newDr, i) => {
        if (index === i) {
            value.driver.name = newDr.name;
            value.driver.age = newDr.age;
            value.driver.gender = newDr.gender;
            value.driver.experience = newDr.experience;
        }
    });
});
// Для початку вкладіть всі наші створені автомобілі в масив cars.
// Далі необхідно рати кожну другу машинку (цикл з кроком в 2), та робити їй підвищення потужності двигуна на 10% та
// ціну на 5%
myCars.forEach((value, index) => {
    if (!(index % 2)) {
        value.power = Math.round(value.power * 1.1);
        value.price = Math.round(value.price * 1.05);
    }
});
// Після того зробити перевірку досвіду ВСІХ наших водіїв. Якщо досвід водія менший за 5 років, але його вік
// більший за 25, то необідно відправити його на курси підвищення кваліфікації, що збільшить йому досвід на 1 рік.
myCars.forEach((value) => {
    let driver = value.driver;
    if (driver.experience < 5 && driver.age > 25) {
        driver.experience = driver.experience + 1;
    }
});
// Також спробуйте порахувати суму, яку потрібно потратити для покупки всіх цих авто в циклі
let priceAllCars = null;
myCars.forEach((value) => {
    priceAllCars = priceAllCars + value.price;
});
// console.log(priceAllCars);
// console.log(myCars);

//

//

//

// Задача: дан отсортированный по возрастанию массив целых чисел. Необходимо вернуть наименьший и наибольший
// индекс заданного элемента.
// Входные данные: arr — массив целых чисел значения которых по модулю не больше 10. Размер массива не более 10
// элементов.
// Вывод: наибольший и наименьший индекс в массиве заданного элемента. Если такого элемента нет в массиве, выведите -1.
// Пример:
// Arr = [1, 2, 3, 4, 4, 4, 4, 7, 7, 9, 14]
// 1. Key = 1
// Answer: MinIndex = 0, MaxIndex = 0.
// 2. Key = 4
// Answer: MinIndex = 3, MaxIndex = 6.
const arr = [1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 7, 7, 9, 14, 14, 14, 14, 15];
const num = +prompt('введіть число');
const some = arr.some((value) => value === num);
if (some) {
    const actualIndex = [];
    arr.forEach((value, index) => {
        if (value === num) {
            actualIndex.push(index);
        }
    });
    actualIndex.forEach((value, index) => {
        if (!index) {
            console.log(`MinIndex = ${value}`);
        }
        if (index === actualIndex.length - 1) {
            console.log(`MaxIndex = ${value}`);
        } else {
        }
    });
} else {
    console.log(-1);
}

//

//

//

//

//
// class Car {
//     constructor(model, madeYear, power, price, driver) {
//         this.model = model;
//         this.madeYear = madeYear;
//         this.power = power;
//         this.price = price;
//         this.driver = driver;
//     }
// }
// class Driver {
//     constructor(name, age, gender, experience) {
//         this.name = name;
//         this.age = age;
//         this.gender = gender;
//         this.experience = experience;
//     }
// }
// const car1 = new Car('bentli', 2015, 420, 55000);
// const car2 = new Car('tesla:', 2020, 355, 65000);
// const car3 = new Car('vaz', 2010, 212, 30000);
// const car4 = new Car('audi', 2012, 263, 15000);
// const car5 = new Car('niva', 2005, 200, 51000);
// const car6 = new Car('opel', 2015, 287, 25000);
// const car7 = new Car('zaz', 2010, 154, 12000);
// const car8 = new Car('mitsubisi', 2015, 312, 5000);
// const car9 = new Car('alfraomeo', 2018, 385, 7000);
// const car10 = new Car('toyota', 2003, 208, 18000);

// const driver1 = new Driver('Max', 32, 'man', 3);
// const driver2 = new Driver('Eduard', 28, 'man', 5);
// const driver3 = new Driver('Alfred', 35, 'man', 12);
// const driver4 = new Driver('Vika', 18, 'woman', 0);
// const driver5 = new Driver('Alina', 25, 'woman', 7);
// const driver6 = new Driver('Anton', 27, 'man', 1);
// const driver7 = new Driver('Dima', 35, 'man', 10);
// const driver8 = new Driver('Ann', 18, 'woman', 0);
// const driver9 = new Driver('Adolf', 20, 'man', 8);
// const driver10 = new Driver('Lisa', 25, 'woman', 5);

// const myCars2 = [car1, car2, car3, car4, car5, car6, car7, car8, car9, car10];
// const myDrivers = [driver1, driver2, driver3, driver4, driver5, driver6, driver7, driver8, driver9, driver10];
