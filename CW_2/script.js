'use strict';
// ============

// зробити масив з 10 чисел [2,17,13,6,22,31,45,66,100,-18]та:
const arrLast = [2, 17, 13, 6, 22, 31, 45, 66, 100, -18];
// 1. перебрати його циклом while
// i = 0;
// while (i < arrLast.length) {
//console.log(arrLast[i]);
// i++;
// }
// 2. перебрати його циклом for
// for (let i = 0; i < arrLast.length; i++) {
//    const element = arrLast[i];
//console.log(element);
// }
// 3. перебрати циклом while та вивести  числа тільки з непарним індексом
// i = 0;
// while (i < arrLast.length) {
//console.log(arrLast[++i]);
// i++;
// }
// 4. перебрати циклом for та вивести  числа тільки з непарним індексом
// for (let i = 0; i < arrLast.length; i++) {
// const element = arrLast[++i];
//console.log(element);
// }
// 5. перебрати циклом while та вивести  числа тільки парні  значення
// i = 0;
// while (i < arrLast.length) {
//console.log(arrLast[i]);
// i = i + 2;
// }
// 6. перебрати циклом for та вивести  числа тільки парні  значення
// for (let i = 0; i < arrLast.length; i = i + 2) {
//    const element = arrLast[i];
//console.log(element);
// }
// 7. замінити кожне число кратне 3 на слово "okten"
// for (let i = 0; i < arrLast.length; i++) {
//    const item = arrLast[i];
//    if (item%3 === 0) {
//       arrLast.splice(i, 1, 'okten');
//    }
// }
// 8. вивести масив в зворотньому порядку.
//console.log(arrLast.reverse())
// 9. всі попередні завдання (окрім 8), але в зворотньому циклі
// i = arrLast.length;
// while (i >= 0) {
//console.log(arrLast[i]);
// i--;
// }
// 9.2. перебрати його циклом for
// for (let i = arrLast.length; i >=0 ; i--) {
//    const element = arrLast[i];
//console.log(element);
// }
// 9.3. перебрати циклом while та вивести  числа тільки з непарним індексом
// i = arrLast.length;
// while (i >=0) {
//    console.log(arrLast[i--]);
//    i--;
// }
// 9.4. перебрати циклом for та вивести  числа тільки з непарним індексом
// for (let i = arrLast.length; i >= 0; i--) {
//    const element = arrLast[i--];
//    console.log(element);
// }
// 9.5. перебрати циклом while та вивести  числа тільки парні  значення
// i = arrLast.length;
// while (i >= 0) {
//    const item = arrLast[i];
//    if (item % 2 === 0) {
//       console.log(item);
//    }
//    i--;
// }
// 9.6. перебрати циклом for та вивести  числа тільки парні  значення
// for (let i = arrLast.length; i >= 0; i--) {
//    const element = arrLast[i];
//    if (element%2 ===0) {
//       console.log(element);
//    }
// }
// 9.7. замінити кожне число кратне 3 на слово "okten"
// for (let i = arrLast.length; i >= 0; i--) {
//    const item = arrLast[i];
//    if (item % 3 === 0) {
//       arrLast.splice(i, 1, 'okten');
//    }
// }
// console.log(arrLast);

//

// 10
//  створити пустий масив та :
// - заповнити його 50 парними числами за допомоги циклу.
// const arrEmpty = [];
// for (let i = arrEmpty.length; 50 > arrEmpty.length; i++) {
//    const num = Math.round(Math.random() * 254);
//    if (num % 2 === 0) {
//       arrEmpty.push(num);
//    }
// }
// console.log(arrEmpty);
// - заповнити його 50 непарними числами за допомоги циклу.
// const arrEmpty = [];
// for (let i = arrEmpty.length; 50 > arrEmpty.length; i++) {
//    const num = Math.round(Math.random() * 254);
//    if (num % 2) {
//       arrEmpty.push(num);
//    }
// }
// console.log(arrEmpty);

//

//

//

// 1 створити пустий масив та :
// const arrEmpty = [];
// 1.1. заповнити його 50 парними числами за допомоги циклу.
// for (let i = arrEmpty.length; 50 > arrEmpty.length; i++) {
//    const num = Math.round(Math.random() * 254);
//    if (num % 2 === 0) {
//       arrEmpty.push(num);
//    }
// }
// console.log(arrEmpty);

// 1.2. заповнити його 50 непарними числами за допомоги циклу.
// for (let i = arrEmpty.length; 50 > arrEmpty.length; i++) {
//    const num = Math.round(Math.random() * 254);
//    if (num % 2) {
//       arrEmpty.push(num);
//    }
// }
// console.log(arrEmpty);

// 1.3. используя Math.random заполнить массив из ???(сколько хотите)
// элементов.
//  диапазон рандома 8 до 732. (но сначала пробуйте БЕЗ ДИАПАЗОНА!)
// for (let i = arrEmpty.length; 10 > arrEmpty.length; i++) {
//    const num = Math.round(Math.random() * 732);
//    if (num > 8) {
//       arrEmpty.push(num);
//    }
// }
//console.log(arrEmpty);

//  2. вывести на консоль  каждый третий елемент
// for (let i = 0; i < arrEmpty.length; i = i + 3) {
//    const item = arrEmpty[i];
//    console.log(`${i} - ${item}`);
// }

//  3. вывести на консоль  каждый третий елемент но при условии что его значение является парным.
// for (let i = 0; i < arrEmpty.length; i = i + 3) {
//    const item = arrEmpty[i];
//    if (item % 2 === 0) {
//       console.log(`${i} - ${item}`);
//    }
// }

//  4. вывести на консоль  каждый третий елементm но при условии что он имеет парное значение и
//  записать их в другой массив.
// const newArr = [];
// for (let i = 0; i < arrEmpty.length; i = i + 3) {
//    const item = arrEmpty[i];
//    if (item % 2 === 0) {
//       console.log(`${i} - ${item}`);
//       newArr.push(item);
//    }
// }
// console.log(newArr);

//

//  5. Вывести каждый елемент массива у которого соседний с права
//  элемент - парный
// for (let i = 0; i < arrEmpty.length; i++) {
//    const item = arrEmpty[i];
//    const neighbor = arrEmpty[i + 1];
//    if (neighbor % 2 === 0) {
//       console.log(item);
//    }
// }

//

//   5 масив з числами [100,250,50,168,120,345,188], Які характеризують
//   вартість окремої покупки. обрахувати середній чек.
// const bills = [100, 250, 50, 168, 120, 345, 188];
// let allPrice = null;
// for (let i = 0; i < bills.length; i++) {
//    const priceOneBill = bills[i];
//    allPrice = allPrice + priceOneBill;
// }
// let averageBill = allPrice / bills.length;
// console.log(averageBill);

//

// 3 створити масив з рандомними значеннями, помножити всі його елементи
//  на 5 та перемістити їх в інший масив.
// const random = [];
// const arra5 = [];
// for (let i = arra5.length; i <= 30; i++) {
//    random.push(Math.round(Math.random() * 43));
// }
// console.log(random);
// for (const i of random) {
//    arra5.push(i * 5);
// }
// console.log(arra5);

//

// 4 створити масив з будь якими значеннями (стрінги, числа, і тд...).
// пройтись по ньому, і якщо об'єкт є числом,
//  додати його в інший масив.
// const array99 = [51, 'js', true, 43, 'css', 34, 12, false, 8, 'jq', 5];
// const new99 = [];
// for (const i of array99) {
//    if (typeof i === 'number') {
//       new99.push(i);
//    }
// }
// console.log(new99);
